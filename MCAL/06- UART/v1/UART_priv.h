/****************************************************************/
/* Author: Mahmoud Alaa Elwelily                                */
/* Date: 27-Apr-19                                              */
/* Version: 01                                                  */
/* Description: Private file for UART Driver for AVR ATmega32   */
/****************************************************************/

/*Preprocessor Guard*/
#ifndef UART_PRIV_H 
#define UART_PRIV_H 

/*Private macros to define the possible UART modes*/
#define UART_u8_NORMAL_MODE              0
#define UART_u8_DBL_SPD_MODE             1
#define UART_u8_DBL_SNQ_MSTR_MODE        2

/*Private macros to define the possible states of UART Transmitter,Receiver and the interrupts*/
#define UART_u8_ENABLE                   (u8)1
#define UART_u8_DISABLE 				 (u8)0

/*Private macros to define the possible parity types*/
#define UART_u8_NO_PARITY                (u8)0
#define UART_u8_EVEN_PARITY              (u8)1
#define UART_u8_ODD_PARITY               (u8)2

/*Private macros to define the number of stop bits and the data size*/
#define UART_u8_ONE_BIT                  (u8)0
#define UART_u8_TWO_BITS                 (u8)1
#define UART_u8_FIVE_BITS                (u8)2
#define UART_u8_SIX_BITS                 (u8)3
#define UART_u8_SEVEN_BITS               (u8)4
#define UART_u8_EIGHT_BITS               (u8)5
#define UART_u8_NINE_BITS                (u8)6

/****************************************************************/
/* Defining UART regisiters                                     */
/****************************************************************/
#define UART_UDR			((Register*) 0x2C) -> ByteAccess

#define UART_UCSRA			((Register*) 0x2B) -> ByteAccess
/*Defining the bits of UART_UCSRA register*/
#define MPCM                 (u8)0
#define U2X                  (u8)1
#define PE                   (u8)2
#define DOR                  (u8)3
#define FE                   (u8)4
#define UDRE                 (u8)5
#define TXC                  (u8)6
#define RXC                  (u8)7


#define UART_UCSRB			((Register*) 0x2A) -> ByteAccess
/*Defining the bits of UART_UCSRB register*/
#define TXB8                 (u8)0
#define RXB8                 (u8)1
#define UCSZ2                (u8)2
#define TXEN                 (u8)3
#define RXEN                 (u8)4
#define UDRIE                (u8)5
#define TXCIE                (u8)6
#define RXCIE                (u8)7

#define UART_UCSRC			((Register*) 0x40) -> ByteAccess
/*Defining the bits of UART_UCSRC register*/
#define UCPOL                (u8)0
#define UCSZ0                (u8)1
#define UCSZ1                (u8)2
#define USBS                 (u8)3
#define UPM0                 (u8)4
#define UPM1                 (u8)5
#define UMSEL                (u8)6
#define URSEL                (u8)7

#define UART_UBRRH			((Register*) 0x40) -> ByteAccess
#define UART_UBRRL			((Register*) 0x29) -> ByteAccess

/************************************************************/
/*      ISRs of UART interrupts                             */
/************************************************************/
/*The ISR of USART, Rx Complete interrupt*/
void __vector_13 (void) __attribute__((signal));

/*The ISR of USART Data Register Empty interrupt*/
void __vector_14 (void) __attribute__((signal));

/*The ISR of USART, Tx Complete interrupt*/
void __vector_15 (void) __attribute__((signal));

#endif /* UART_PRIV_H */ 
