/**************************************************************/
/* Author: Mahmoud Alaa Elwelily                              */
/* Date: 14-May-19                                            */
/* Version: 01                                                */
/* Description: Program file for SPI Driver for AVR ATmega32  */
/**************************************************************/
#include "STD_TYPES.h" 
#include "BIT_CALC.h" 
#include "SPI_interface.h" 
#include "SPI_priv.h" 
#include "SPI_config.h" 

/*Static pointer to the passed string needed to be sent using the Asynchronous transmitting function*/
static u8 *SPI_pu8TransmittedString;

/*Static pointer to the passed array needed to be filled using the Asynchronous receiving function*/
static u8 *SPI_pu8ReceivedString;

/*Static variable that will be equal the required length of the received msg*/
static u8 SPI_u8NoOfBytes;

/* Incremented index using in asynchronous transmitting and receiving */
static u8 SPI_u8Index;

/* Static Global callback function whic refers to the function needed to be called after finishing transmission the buffer*/
static void (*PtrASychCallback) (void) = NULL;

/*
 * Description: Function to initialize the SPI peripheral
 * Inputs: none
 * Output: void
 */
void SPI_vInit (void)
{
	if (SPI_u8_SELECTROLE == SPI_u8_MASTER)
	{
		SET_BIT(SPI_SPCR,MSTR);
	}

	if (SPI_u8_DATA_ORDER == SPI_u8_LSB)
	{
		SET_BIT(SPI_SPCR,DORD);
	}

	if (SPI_u8_CLKPOL == SPI_u8_FALLINGEDGE)
	{
		SET_BIT(SPI_SPCR,CPOL);
	}

	if (SPI_u8_CLKPHASE == SPI_u8_SETUP)
	{
		SET_BIT(SPI_SPCR,CPHA);
	}

	SPI_SPCR |= SPI_u8_SPRBITS;

	  //if Double SPI Speed is selected
	if (SPI_u8_CLKRATE > 3)
		SET_BIT(SPI_SPSR,SPI2X);

	SET_BIT(SPI_SPCR,SPE);
	return;
}

/*
 * Description: Synchronous Function to send and receive a byte using polling mechanism
 * Inputs: the byte needed to be sent and a pointer to received byte
 * Output: the Error state of the function
 */
u8 SPI_u8SendReceiveByte_Synch (u8 Copy_u8SentByte,u8 *Copy_pu8RecByte)
{
	/*Local Variable holding the error state*/
	u8 Local_u8Error = ERROR_NOK;
	u16 Local_u16Timeout = 0;

	if (Copy_pu8RecByte != NULL)
	{
		Local_u8Error = ERROR_OK;
		SPI_SPDR = Copy_u8SentByte;
		while (GET_BIT(SPI_SPSR,SPIF) == 0 && Local_u16Timeout < SPI_u16_FAULT_TIMEOUT)
		{
			Local_u16Timeout++;
		}
		if (Local_u16Timeout == SPI_u16_FAULT_TIMEOUT)
			return ERROR_NOK;
		*Copy_pu8RecByte = SPI_SPDR;
	}
	/*Function return*/
	return Local_u8Error;
}

/*
 * Description: Asynchronous Function to start sending a string using Serial Transfer Complete interrupt
 * Inputs: (1) pointer to array of characters needed to be sent
 * 		   (2) pointer to array of characters that is supposed to be received
 * 		   (3) the expected size of the transmitted and received buffer
 * 		   (4) the callback function to notify the upper layer that the complete message is successfully sent
 * Output: the Error state of the function
 */
u8 SPI_u8SendReceiveBuffer_Asynch (u8 *Copy_pu8SentBuffer,u8 *Copy_pu8RecBuffer,u8 Copy_u8NoOfBytes,void (*CallBack)(void))
{
	/*Local Variable holding the error state*/
	u8 Local_u8Error = ERROR_NOK;

	if (Copy_pu8SentBuffer != NULL && Copy_pu8RecBuffer != NULL && CallBack != NULL)
	{
		Local_u8Error = ERROR_OK;
		SPI_pu8TransmittedString = Copy_pu8SentBuffer;
		SPI_pu8ReceivedString = Copy_pu8RecBuffer;
		PtrASychCallback = CallBack;
		SPI_u8NoOfBytes = Copy_u8NoOfBytes;
		SPI_SPDR = SPI_pu8TransmittedString[SPI_u8Index];
		SET_BIT(SPI_SPCR,SPIE);
	}
	/*Function return*/
	return Local_u8Error;
}

/* The ISR of Serial Transfer Complete */
void __vector_12 (void)
{
	SPI_pu8ReceivedString[SPI_u8Index] = SPI_SPDR;
	if (SPI_u8Index == SPI_u8NoOfBytes)
	{
		SPI_u8Index = 0;
		SPI_u8NoOfBytes = 0;
		CLEAR_BIT(SPI_SPCR,SPIE);
		PtrASychCallback();
	}
	else
	{
		SPI_u8Index++;
		SPI_SPDR = SPI_pu8TransmittedString[SPI_u8Index];
	}
	return;
}
