/***************************************************************/
/* Author: Mahmoud Alaa Elwelily                               */
/* Date: 14-May-19                                             */
/* Version: 01                                                 */
/* Description: Private file for SPI Driver for AVR ATmega32   */
/***************************************************************/

/*Preprocessor Guard*/
#ifndef SPI_PRIV_H 
#define SPI_PRIV_H 

/* Private macros to define the possible state of Serial Transfer Complete interrupt */
#define SPI_u8_ENABLE         (u8)0
#define SPI_u8_DISABLE        (u8)1

/* Private macros to define the possible order by which the byte will be sent */
#define SPI_u8_LSB            (u8)0
#define SPI_u8_MSB            (u8)1

/* Private macros to define the states of an SPI node (Master/Slave) */
#define SPI_u8_MASTER         (u8)0
#define SPI_u8_SLAVE          (u8)1

/* Private macros to define the clock polarity */
#define SPI_u8_RISINGEDGE     (u8)0
#define SPI_u8_FALLINGEDGE    (u8)1

/* Private macros to define the clock phase */
#define SPI_u8_SAMPLE         (u8)0
#define SPI_u8_SETUP          (u8)1


/* Private macros to define the SPI clock rate */
#define SPI_OVER_4         0UL
#define SPI_OVER_16        1UL
#define SPI_OVER_64        2UL
#define SPI_OVER_128       3UL
#define SPI_OVER_2_HSPD    4UL
#define SPI_OVER_8_HSPD    5UL
#define SPI_OVER_32_HSPD   6UL
#define SPI_OVER_64_HSPD   7UL

/****************************************************************/
/* Defining SPI regisiters                                     */
/****************************************************************/
#define SPI_SPCR			((Register*) 0x2D) -> ByteAccess
/*Defining the bits of SPI_SPCR register*/
#define SPR0                 (u8)0
#define SPR1                 (u8)1
#define CPHA                 (u8)2
#define CPOL	             (u8)3
#define MSTR	             (u8)4
#define DORD                 (u8)5
#define SPE                  (u8)6
#define SPIE                 (u8)7

#define SPI_SPSR			((Register*) 0x2E) -> ByteAccess
/*Defining the bits of SPI_SPSR register*/
#define SPI2X                 (u8)0
#define SPIF                  (u8)7

#define SPI_SPDR			((Register*) 0x2F) -> ByteAccess

/************************************************************/
/*      ISRs of SPI interrupts                             */
/************************************************************/
/*The ISR of Serial Transfer Complete*/
void __vector_12 (void) __attribute__((signal));

#endif /* SPI_PRIV_H */ 
