/******************************************************************/
/* Author: Mahmoud Alaa Elwelily                                  */
/* Date: 14-May-19                                                */
/* Version: 01                                                    */
/* Description: Interface file for SPI Driver for AVR ATmega32    */
/******************************************************************/

/*Preprocessor Guard*/
#ifndef SPI_INTERFACE_H 
#define SPI_INTERFACE_H 

/*
 * Description: Function to initialize the SPI peripheral
 * Inputs: none
 * Output: void
 */
void SPI_vInit (void);

/*
 * Description: Synchronous Function to send and receive a byte using polling mechanism
 * Inputs: the byte needed to be sent and a pointer to received byte
 * Output: the Error state of the function
 */
u8 SPI_u8SendReceiveByte_Synch (u8 Copy_u8SentByte,u8 *Copy_pu8RecByte);

/*
 * Description: Asynchronous Function to start sending a string using Serial Transfer Complete interrupt
 * Inputs: (1) pointer to array of characters needed to be sent
 * 		   (2) pointer to array of characters that is supposed to be received
 * 		   (3) the expected size of the transmitted and received buffer
 * 		   (4) the callback function to notify the upper layer that the complete message is successfully sent
 * Output: the Error state of the function
 */
u8 SPI_u8SendReceiveBuffer_Asynch (u8 *Copy_pu8SentBuffer,u8 *Copy_pu8RecBuffer,u8 Copy_u8NoOfBytes,void (*CallBack)(void));



#endif /* SPI_INTERFACE_H */ 
