/****************************************************************************/
/* Author: Mahmoud Alaa Elwelily                                            */
/* Date: 14-Apr-19                                                          */
/* Version: 01                                                              */
/* Description: Interface file for ADC Driver compatible with AVR ATmega32  */
/****************************************************************************/

/*Preprocessor Guard*/
#ifndef ADC_INTERFACE_H 
#define ADC_INTERFACE_H 

/*Macro to define the availabe Multiplexed ADC channels*/
#define ADC_u8ADC0_SEL     (u8)0
#define ADC_u8ADC1_SEL     (u8)1
#define ADC_u8ADC2_SEL     (u8)2
#define ADC_u8ADC3_SEL     (u8)3
#define ADC_u8ADC4_SEL     (u8)4
#define ADC_u8ADC5_SEL     (u8)5
#define ADC_u8ADC6_SEL     (u8)6
#define ADC_u8ADC7_SEL     (u8)7

/*
 * Description: Function to initialize the ADC driver
 * Inputs: none
 * Output: void
 */
void ADC_vInit (void);

/*
 * Description: Function to get the ADC value from the required ADc channel using Blocking method
 * Inputs: ADC channel number and the Pointer to the variable supposed to hold the ADC variable
 * Output: Error state
 */
u8 ADC_u8GetADCBlocking (u8 Copy_u8ChannelNb,u16* Copy_pu16ADCValue);

/*
 * Description: Function to get the ADC value from the required ADc channel using Non Blocking method
 * Inputs: ADC channel number and the Pointer to the variable supposed to hold the ADC variable
 * Output: Error State
 */
u8 ADC_u8GetADCNonBlocking (u8 Copy_u8ChannelNb,u16* Copy_pu16ADCValue);

/*
 * Description: Function to start the process of ADC conversion from multiple number of ADC channels
 * Inputs: void
 * Output: Error state
 */
u8 ADC_vADCRefresh (void);

/*
 * Description: Function to get the ADC of a certain ADC channel in case of applying ADC Chaining process
 * on multiple number of ADC channels
 * Inputs: The number of ADc channel required to get its value and the Pointer to the variable supposed to hold the ADC variable
 * Output: Error state
 */
u8 ADC_u8GetADCValue (u8 Copy_u8ChannelNbInChain,u16* Copy_pu16ADCValue);
/*
 * Description: Function to set the callback function which will run as ISR on ADC conversion complete
 * Inputs: a pointer to the callback function
 * Output: the Error state of the function
 */
u8 ADC_u8SetCallBack (void(*Copy_PVCallBack)(void));

#endif /* ADC_INTERFACE_H */ 
