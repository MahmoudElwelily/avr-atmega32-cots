/*********************************************************************************/
/* Author: Mahmoud Alaa Elwelily                                                 */
/* Date: 14-Apr-19                                                               */
/* Version: 01                                                				     */
/* Description: Configuration file for ADC Driver compatible with AVR ATmega32   */
/*********************************************************************************/

/*Preprocessor Guard*/
#ifndef ADC_CONFIG_H 
#define ADC_CONFIG_H 

/*
Macro to define the initial ADC state
Range:   ADC_u8_ENABLE
	   	 ADC_u8_DISABLE
*/
#define ADC_u8_INIT_STATE            ADC_u8_ENABLE

/*
 Macro to select the ADC resolution
 Range:   ADC_u8_8BITS
	   	  ADC_u8_10BITS
 */
#define ADC_u8_RESOLUTION      ADC_u8_8BITS

/*
Macros for the prescaler of ADC
Range:   ADC_u8_PRESCALER_OVER_2
	   	 ADC_u8_PRESCALER_OVER_4
	   	 ADC_u8_PRESCALER_OVER_8
		 ADC_u8_PRESCALER_OVER_16
		 ADC_u8_PRESCALER_OVER_32
		 ADC_u8_PRESCALER_OVER_64
		 ADC_u8_PRESCALER_OVER_128
*/
#define ADC_u8_PRESCALER	  ADC_u8_PRESCALER_OVER_2

/*
Macros for the select the Vref of the ADC peripheral
Range:   ADC_u8_AREF
	   	 ADC_u8_AVCC
	   	 ADC_u8_INTERNAL_VREF
*/
#define ADC_u8_VREF_SELECT	   ADC_u8_AVCC

/*
Macro to select the Adjacement
Range:   ADC_u8_LEFT_ADJ
	   	 ADC_u8_RIGHT_ADJ
*/
#define ADC_u8_ADJACEMENT       ADC_u8_LEFT_ADJ

/*
Macro to select the ADC interrupt initial state
Range:   ADC_u8_ENABLE
	   	 ADC_u8_DISABLE
*/
#define ADC_u8_INIT_INT	    ADC_u8_ENABLE

/*Macro to define the number of active ADC channels*/
#define ADC_u8_NO_ACTIVE_CHANNELS     (u8)7

/*
Macro to select the ISR running mode whether it will run to measure a sequence of
ADC channels or run a user defined ISR
Range:		ADC_u8_ADC_CHAINING
			ADC_u8_USER_DEF_ISR
*/
#define ADC_u8_INT_MODE			ADC_u8_ADC_CHAINING

/*
Macros to define the start and end channel in case of using Chain conversion
Range:   ADC_u8_CHANNEL0
	   	 ADC_u8_CHANNEL1
	   	 ADC_u8_CHANNEL2
		 ADC_u8_CHANNEL3
		 ADC_u8_CHANNEL4
		 ADC_u8_CHANNEL5
		 ADC_u8_CHANNEL6
		 ADC_u8_CHANNEL7
*/
#define ADC_u8_CHAIN_1ST_CHANNEL     ADC_u8_CHANNEL0
#define ADC_u8_CHAIN_LAST_CHANNEL    ADC_u8_CHANNEL7

#endif /* ADC_CONFIG_H */ 

