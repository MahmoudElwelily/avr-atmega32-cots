/*****************************************************************************/
/* Author: Mahmoud Alaa Elwelily                              				 */
/* Date: 14-Apr-19                                            				 */
/* Version: 01                                                				 */
/* Description: Program file for ADC Driver compatible with AVR ATmega32     */
/*****************************************************************************/

#include "STD_TYPES.h" 
#include "BIT_CALC.h" 
#include "ADC_interface.h" 
#include "ADC_priv.h" 
#include "ADC_config.h" 

/* pointer to the callback function which  which will run as ISR on ADC conversion complete*/
static void (*PVCallBack) (void) = NULL;

/*Static global variable holding the number of ADC chained channels*/
static u8 ADC_u8ChainSize;

/*Static global array holding the ADC of each chained ADC channel*/
static u8 ADC_Au16ADCValues[ADC_u8_CHAIN_LAST_CHANNEL-ADC_u8_CHAIN_1ST_CHANNEL];

static u8 ADC_u8RefreshFlag = 0;

/*
 * Description: Function to initialize the ADC driver
 * Inputs: none
 * Output: void
 */
void ADC_vInit (void)
{
	#if (ADC_u8_INIT_STATE == ADC_u8_ENABLE)
	{
		switch (ADC_u8_PRESCALER)
		{
			case ADC_u8_PRESCALER_OVER_4:
			{

				SET_BIT(ADC_ADCSRA,ADPS1);
				break;
			}
			case ADC_u8_PRESCALER_OVER_8:
			{
				SET_BIT(ADC_ADCSRA,ADPS0);
				SET_BIT(ADC_ADCSRA,ADPS1);
				break;
			}
			case ADC_u8_PRESCALER_OVER_16:
			{
				SET_BIT(ADC_ADCSRA,ADPS2);
				break;
			}
			case ADC_u8_PRESCALER_OVER_32:
			{
				SET_BIT(ADC_ADCSRA,ADPS0);
				SET_BIT(ADC_ADCSRA,ADPS2);
				break;
			}
			case ADC_u8_PRESCALER_OVER_64:
			{
				SET_BIT(ADC_ADCSRA,ADPS0);
				SET_BIT(ADC_ADCSRA,ADPS1);
				break;
			}
			case ADC_u8_PRESCALER_OVER_128:
			{

				SET_BIT(ADC_ADCSRA,ADPS1);
				SET_BIT(ADC_ADCSRA,ADPS2);
				break;
			}
		}

		if (ADC_u8_VREF_SELECT == ADC_u8_AVCC)
		{
			SET_BIT(ADC_ADMUX,REFS0);
		}
		else if (ADC_u8_VREF_SELECT == ADC_u8_INTERNAL_VREF)
		{
			SET_BIT(ADC_ADMUX,REFS0);
			SET_BIT(ADC_ADMUX,REFS1);
		}

		if (ADC_u8_ADJACEMENT == ADC_u8_LEFT_ADJ)
			SET_BIT(ADC_ADMUX,ADLAR);

		if (ADC_u8_INIT_INT == ADC_u8_ENABLE)
			SET_BIT(ADC_ADCSRA,ADIE);

		SET_BIT(ADC_ADCSRA,ADEN);
	}
	#endif
	return;
}


/*
 * Description: Function to get the ADC value from the required ADc channel using Blocking method
 * Inputs: ADC channel number and the Pointer to the variable supposed to hold the ADC variable
 * Output: Error state
 */
u8 ADC_u8GetADCBlocking (u8 Copy_u8ChannelNb,u16* Copy_pu16ADCValue)
{
	/*Local Variable holding the error state*/
	 u8 Local_u8Error = ERROR_OK;
	if (Copy_u8ChannelNb < ADC_u8_MAX_NO_ADC_CHANNELS && Copy_pu16ADCValue != NULL)
	{
		/* Clear the ADC Mux selection Bits */
		ADC_ADMUX &= ~(31<<0);
		/*Select the Mux selection bit according to the required ADC channel*/
		ADC_ADMUX |= ADC_Au8MuxPinSelect[Copy_u8ChannelNb];

		/*Start ADC conversion*/
		SET_BIT(ADC_ADCSRA,ADSC);

		CLEAR_BIT(ADC_ADCSRA,ADIE);
		while(GET_BIT(ADC_ADCSRA,ADIF) == 0);   	/*wait until the conversion ends */
		SET_BIT(ADC_ADCSRA,ADIF);
		SET_BIT(ADC_ADCSRA,ADIE);

		if (ADC_u8_RESOLUTION == ADC_u8_8BITS)
			*Copy_pu16ADCValue = ADC_ADCH;
		else
			*Copy_pu16ADCValue = ADC_ADCVAL_10BITS;
	}
	else
	{
		Local_u8Error = ERROR_NOK;
	}
	/*Function return*/
	return Local_u8Error;
}

/*
 * Description: Function to get the ADC value from the required ADc channel using Non Blocking method
 * Inputs: ADC channel number and the Pointer to the variable supposed to hold the ADC variable
 * Output: Error State
 */
u8 ADC_u8GetADCNonBlocking (u8 Copy_u8ChannelNb,u16* Copy_pu16ADCValue)
{
	/*Local Variable holding the error state*/
	u8 Local_u8Error = ERROR_OK;
	if (Copy_u8ChannelNb < ADC_u8_MAX_NO_ADC_CHANNELS && Copy_pu16ADCValue != NULL)
	{

	}
	else
	{
		Local_u8Error = ERROR_NOK;
	}
	/*Function return*/
	return Local_u8Error;
}

/*
 * Description: Function to start the process of ADC conversion from multiple number of ADC channels
 * It is an Asynqronous function which initiates a series of ISRs
 * Inputs: void
 * Output: void
 */
u8 ADC_u8ADCRefresh (void)
{
	/*Local Variable holding the error state*/
	u8 Local_u8Error = ERROR_OK;

	if (ADC_u8RefreshFlag == 0)
	{
		ADC_u8RefreshFlag = 1;

		ADC_u8ChainSize = ADC_u8_CHAIN_LAST_CHANNEL - ADC_u8_CHAIN_1ST_CHANNEL;

		/* Clear the ADC Mux selection Bits */
		ADC_ADMUX &= ~(31<<0);

		/*Select the Mux selection bit according to the Starting channel in the chain*/
		ADC_ADMUX |= ADC_Au8MuxPinSelect[ADC_u8_CHAIN_1ST_CHANNEL];

		/*Start ADC conversion*/
		SET_BIT(ADC_ADCSRA,ADSC);
	}
	else
	{
		Local_u8Error = ERROR_NOK;
	}
	return Local_u8Error;
}

/*
 * Description: Function to get the ADC of a certain ADC channel in case of applying ADC Chaining process
 * on multiple number of ADC channels
 * Inputs: The number of ADC channel (in the ADC Chain) required to get its value and the Pointer to the variable supposed to hold the ADC variable
 * Output: Error state
 */
u8 ADC_u8GetADCValue (u8 Copy_u8ChannelNbInChain,u16* Copy_pu16ADCValue)
{
	/*Local Variable holding the error state*/
	u8 Local_u8Error = ERROR_OK;
	if (Copy_u8ChannelNbInChain < ADC_u8ChainSize && Copy_pu16ADCValue != NULL && ADC_u8RefreshFlag == 0)
	{
		*Copy_pu16ADCValue = ADC_Au16ADCValues[Copy_u8ChannelNbInChain];
	}
	else
	{
		Local_u8Error = ERROR_NOK;
	}
	/*Function return*/
	return Local_u8Error;
}

/*
 * Description: Function to set the callback function which will run as ISR on ADC conversion complete
 * Inputs: a pointer to the callback function
 * Output: the Error state of the function
 */
u8 ADC_u8SetCallBack (void(*Copy_PVCallBack)(void))
{
	/*Local Variable holding the error state*/
	u8 Local_u8Error = ERROR_OK;

	if (Copy_PVCallBack != NULL)
	{
		PVCallBack = Copy_PVCallBack;
	}
	else
	{
		Local_u8Error = ERROR_NOK;
	}
	/*Function return*/
	return Local_u8Error;
}

void __vector_16 (void)
{
	static u8 Local_u8Counter = 0;
	static u8 Local_u8ChannelIndex = ADC_u8_CHAIN_1ST_CHANNEL;

	if (PVCallBack != NULL)
	{
		if (ADC_u8_INT_MODE == ADC_u8_ADC_CHAINING)
		{
			if (ADC_u8_RESOLUTION == ADC_u8_8BITS)
			 	 ADC_Au16ADCValues[Local_u8Counter] = ADC_ADCH;
			else
				ADC_Au16ADCValues[Local_u8Counter] = ADC_ADCVAL_10BITS;

			Local_u8Counter++;

			if (Local_u8Counter == ADC_u8ChainSize)
			{
				CLEAR_BIT(ADC_ADCSRA,ADIE);
				ADC_u8RefreshFlag = 0;
				Local_u8Counter = 0;
				Local_u8ChannelIndex = ADC_u8_CHAIN_1ST_CHANNEL;
				PVCallBack();
			}
			else
			{
				Local_u8ChannelIndex++;
				/* Clear the ADC Mux selection Bits */
				ADC_ADMUX &= ~(31<<0);
				/*Select the Mux selection bit according to the required ADC channel*/
				ADC_ADMUX |= ADC_Au8MuxPinSelect[Local_u8ChannelIndex];
				/*Start ADC conversion*/
				SET_BIT(ADC_ADCSRA,ADSC);
			}
		}
		else
			PVCallBack();
	}
}
