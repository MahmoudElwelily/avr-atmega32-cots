/*****************************************************************/
/* Author: Mahmoud Alaa Elwelily                                 */
/* Date: 28-Mar-19                                        		 */
/* Version: 01                                            	   	 */
/* Description: Interface file for Timer Driver for AVR ATmega32 */
/*****************************************************************/

/*Preprocessor Guard*/
#ifndef TMR_INTERFACE_H 
#define TMR_INTERFACE_H

/*Macro for defining the 3 availabe timers in AVR ATmega32*/
#define TMR_u8_TIMER0      (u8)0
#define TMR_u8_TIMER1      (u8)1
#define TMR_u8_TIMER2      (u8)2

#define TMR_u32_1SEC       (u32)1000000
#define TMR_u32_1MSEC      (u32)1000

/*
 * Description: Function to set the initialized state and prescaler for each timer (0,1,2)
 * Inputs: none
 * Output: void
 */
void TMR_vInit (void);

/*
 * Description: Function to enable one of the available timers in AVR ATmega32 (0,1,2)
 * Inputs: Timer Number
 * Output: the Error state of the function
 */
u8 TMR_u8EnableTimer (u8 Copy_u8TimerNb);

/*
 * Description: Function to disable one of the available timers in AVR ATmega32 (0,1,2)
 * Inputs: Timer Number
 * Output: the Error state of the function
 */
u8 TMR_u8DisableTimer (u8 Copy_u8TimerNb);

/*
 * Description: Function to set the duty cycle of the PWM signal generated from Timers(0,1,2)
 * Inputs: Timer Number and the desired duty cycle
 * Output: the Error state of the function
 */
u8 TMR_u8SetDutyCycle (u8 Copy_u8TimerNb,u8 Copy_u8DutyCycle);

/*
 * Description: Function to set the duty cycle of the PWM signal generated from Timer1,Pins OC1A or/and OC1B
 * (used mainly in servo interfacing)
 * Inputs: The desired timer compare value
 * Output: void
 */
void TMR_u8SetPWM (u16 Copy_u16ReqCompValue);

/*
 * Description: Function to set the delay time for the required timer
 * this function can be used for making delay (in case of Timer normal mode)
 * or for specifying a sampling time (in case of Timer CTC mode)
 * Inputs: the delay time in microsecond
 * Output: the Error state of the function
 */
u8 TMR_u8SetDesiredTime (u8 Copy_u8TimerNb,u32 Copy_u32TimeInUs);

/*
 * Description: Function to set the callback function for a certain timer in case of firing its interrupt
 * Inputs: the number of the available timer (0,1,2) and a pointer to its callback function
 * Output: the Error state of the function
 */
u8 TMR_u8SetCallBack (u8 Copy_u8TimerNb,void(*Copy_PVCallBack)(void));

/************************************************************/
/*      Functions related to Input Capture Mode             */
/************************************************************/
/*
 * Description: Function to enable input capture interrupt
 * Inputs: void
 * Output: nothing
 */
void TMR_u8EnableIcuInterrupt (void);

/*
 * Description: Function to disable input capture interrupt
 * Inputs: void
 * Output: nothing
 */
void TMR_u8DisbleIcuInterrupt (void);

/*
* Description: Function to get the Period of the signal received by the input capture pin ICP1 (D6)
* Inputs: the container variable which will hold the value of the period
* Output: the Error state of the function
*/
u8 TMR_u8GetPeriod (u16 *Copy_Pu16Period);

/*
* Description: Function to get the Duty Cycle of the signal received by the input capture pin ICP1 (D6)
* Inputs: the container variable which will hold the value of the Duty Cycle
* Output: the Error state of the function
*/
u8 TMR_u8GetDutyCycle (u16 *Copy_Pu16DutyCycle);

/*
* Description: Function to get the OnTime of the signal received by the input capture pin ICP1 (D6)
* Inputs: the container variable which will hold the value of the OnTime
* Output: the Error state of the function
*/
u8 TMR_u8OnTime (u16 *Copy_Pu16OnTime);

/*
* Description: Function to get the OffTime of the signal received by the input capture pin ICP1 (D6)
* Inputs: the container variable which will hold the value of the OffTime
* Output: the Error state of the function
*/
u8 TMR_u8OffTime (u16 *Copy_Pu16OffTime);

#endif /* TMR_INTERFACE_H */ 
