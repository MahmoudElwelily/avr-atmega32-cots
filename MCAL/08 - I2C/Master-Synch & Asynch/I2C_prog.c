/**************************************************************/
/* Author: MAhmoud Alaa Elwelily                              */
/* Date: 18-May-19                                            */
/* Version: 01                                                */
/* Description: Program file for I2C Driver for AVR ATmega32  */
/**************************************************************/
#include "STD_TYPES.h" 
#include "BIT_CALC.h" 
#include "I2C_interface.h" 
#include "I2C_priv.h" 
#include "I2C_config.h" 


/*Static pointer to the passed string needed to be sent using the Asynchronous transmitting function*/
static u8 *I2C_pu8TransmittedString;

/*Static variable that will be equal the required length of the received msg*/
static u8 I2C_u8NoOfBytes;

static u8 I2C_u8SlaveAddress;

/* Incremented index using in asynchronous transmitting and receiving */
static u8 I2C_u8Count;

/* Static Global callback function whic refers to the function needed to be called after finishing transmission the buffer*/
static void (*PtrASychCallback) (void) = NULL;

static u8 I2C_u8State;

/*
 * Description: Function to initialize the I2C peripheral
 * Inputs: none
 * Output: void
 */
void I2C_vInit (void)
{
	I2C_TWBR = I2C_BIT_RATE;

	/* Checking the I2C Prescaler Bits */
	switch (I2C_PRESCALER_BITS)
	{
		case I2C_u8_PRESCALER_1:
		{
			 CLEAR_BIT(I2C_TWSR,TWPS0);
			 CLEAR_BIT(I2C_TWSR,TWPS1);
			break;
		}
		case I2C_u8_PRESCALER_4:
		{
			SET_BIT(I2C_TWSR,TWPS0);
			CLEAR_BIT(I2C_TWSR,TWPS1);
			break;
		}
		case I2C_u8_PRESCALER_16:
		{
			CLEAR_BIT(I2C_TWSR,TWPS0);
			SET_BIT(I2C_TWSR,TWPS1);
			break;
		}
		case I2C_u8_PRESCALER_64:
		{
			SET_BIT(I2C_TWSR,TWPS0);
			SET_BIT(I2C_TWSR,TWPS1);
			break;
		}
	}

	/* Checking the state of the Acknowledge Bit */
	if (I2C_ACK_BIT_STATE == I2C_u8_ENABLE)
	{
		SET_BIT(I2C_TWCR,TWEA);
	}

	/* Checking the state of the Two-wire Serial Interface Interrupt */
	if (I2C_u8_INT_STATE == I2C_u8_ENABLE)
	{
		SET_BIT(I2C_TWCR,TWINT);
	}

	I2C_TWAR = I2C_SLAVE_u8_ADDRESS;
	CLEAR_BIT(I2C_TWAR,TWGCE);
	/* Checking the state of the recognition of a General Call given over the Two-wire Serial Bus */
	if (I2C_GEN_CALL_RECOG_STATE == I2C_u8_ENABLE)
	{
		SET_BIT(I2C_TWAR,TWGCE);
	}

	SET_BIT(I2C_TWCR,TWEN);

	return;
}

/*
 * Description: SynchronousFunction to send a buffer using polling methodology on Two-wire Serial INT flag
 * Inputs: (1) pointer to array of characters needed to be sent
 * 		   (2) the size of the transmitted buffer
 * 		   (3) the address of the slave on which the master wants to write
 * Output: the error state of the function
 */
u8 I2C_u8SendSynch (u8 *Copy_pu8data,u8 Copy_u8BufferLength,u8 Copy_u8SlaveAddress)
{
	/*Local Variable holding the error state*/
	u8 Local_u8Error = ERROR_NOK;
	u8 Local_u8State = I2C_START;
	u8 Local_u8Success = 1;
	u8 Local_u8Done = 0;
	u8 Local_u16TimeOut = 0;
	u8 Local_u8Counter = 0;
	u8 Local_u8Address = Copy_u8SlaveAddress;;

	if (Copy_pu8data != NULL)
	{
		while ((Local_u8Success == 1) && (Local_u8Done == 0))
		{
			switch (Local_u8State)
			{
				/* Sending Start condition */
				case I2C_START:
					CLEAR_I2C_INT_FLAG();
					GEN_START_COND();
					while ((GET_BIT(I2C_TWCR,TWINT) == 0) && (Local_u16TimeOut < I2C_u16_FAULT_TIMEOUT))
					{
						Local_u16TimeOut++;
					}
					/* the while loop is broken because of the INT flag */
					if (GET_BIT(I2C_TWCR,TWINT) == 1)
					{
						if ((I2C_TWSR & 0xF8) == START_SENT)
						{
							CLEAR_I2C_INT_FLAG();
							Local_u16TimeOut = 0;
							Local_u8State = I2C_SEND_ADDRESS;
						}
					}
					/* the while loop is broken because of the timeout */
					else
					{
						Local_u8Success = 0;
					}
					break;

				/* Sending the slave address + write operation */ (add the timeout checking here)
				case I2C_SEND_ADDRESS:
					Local_u8Address &= ~(1<<0);
					I2C_TWDR = Local_u8Address;
					while (GET_BIT(I2C_TWCR,TWINT) == 0);
					if ((I2C_TWSR & 0xF8) == MT_SLA_ACK)
					{
						CLEAR_I2C_INT_FLAG();
						Local_u8State = I2C_SEND_DATA;
					}
					else if ((I2C_TWSR & 0xF8) == MT_SLA_NACK)
					{
						Local_u8Success = 0;
					}
					break;

				/* Sending data operation */
				case I2C_SEND_DATA:
					I2C_TWDR = Copy_pu8data[Local_u8Counter];
					Local_u8Counter++;
					while (GET_BIT(I2C_TWCR,TWINT) == 0 && (Local_u16TimeOut < I2C_u16_FAULT_TIMEOUT))
					{
						Local_u16TimeOut++;
					}
					if ((I2C_TWSR & 0xF8) == MT_DATA_ACK)
					{
						CLEAR_I2C_INT_FLAG();
						if (Local_u8Counter < Copy_u8BufferLength)
							Local_u8State = I2C_SEND_DATA;
						else
							Local_u8State = I2C_STOP;
					}
					else if (((I2C_TWSR & 0xF8) == MT_DATA_NACK) || (Local_u16TimeOut == I2C_u16_FAULT_TIMEOUT))
					{
						Local_u8Success = 0;
					}
					break;
				case I2C_STOP:
					GEN_STOP_COND();
					Local_u8Done = 1;
					break;
			}
		}
	}
	else
		Local_u8Done = 0;

	if (Local_u8Done == 1)
		Local_u8Error = ERROR_OK;
	else
		Local_u8Error = ERROR_NOK;

	return Local_u8Error;
}

/*
 * Description: Asynchronous Function to start sending a string using Two-wire Serial Interface Interrupt
 * Inputs: (1) pointer to array of characters needed to be sent
 * 		   (2) the size of the transmitted buffer
 * 		   (3) the address of the slave on which the master wants to write
 * 		   (4) the callback function to notify the upper layer that the complete message is successfully sent
 * Output: the Error state of the function
 */
u8 I2C_u8SendAsynch (u8 *Copy_pu8data,u8 Copy_u8BufferLength,u8 Copy_u8SlaveAddress, void (*Callback)(void))
{
	/*Local Variable holding the error state*/
	u8 Local_u8Error = ERROR_NOK;

	if ((Copy_pu8data != NULL) && (Callback != NULL) )
	{
		I2C_pu8TransmittedString = Copy_pu8data;
		I2C_u8NoOfBytes = Copy_u8BufferLength;
		I2C_u8SlaveAddress = Copy_u8SlaveAddress;
		PtrASychCallback = Callback;
		I2C_u8State = I2C_START;
		CLEAR_I2C_INT_FLAG();
		GEN_START_COND();
	}
	return Local_u8Error;
}

/* The ISR of Two-wire Serial Interface Interrupt */
void __vector_19 (void)
{
	CLEAR_I2C_INT_FLAG();
	switch (I2C_u8State)
	{
		/* In case of Start condition state */
		case I2C_START:
			I2C_u8State = I2C_SEND_ADDRESS;
			I2C_u8SlaveAddress &= ~(1<<0);
			I2C_TWDR = I2C_u8SlaveAddress;
			break;

		/* In case of sending slave address state */
		case I2C_SEND_ADDRESS:
			I2C_u8State = I2C_SEND_DATA;
			I2C_TWDR = I2C_pu8TransmittedString[I2C_u8Count];
			break;

		/* In case of sending data state */
		case I2C_SEND_DATA:
			if (I2C_u8Count < I2C_u8NoOfBytes)
			{
				I2C_u8State = I2C_SEND_DATA;
				I2C_u8Count++;
				I2C_TWDR = I2C_pu8TransmittedString[I2C_u8Count];
			}
			else
			{
				I2C_u8State = I2C_START;
				I2C_u8Count = 0;
				GEN_STOP_COND();
				PtrASychCallback();
			}
			break;
	}
	return;
}
