/*********************************************************************/
/* Author: MAhmoud Alaa Elwelily                                     */
/* Date: 18-May-19                                                   */
/* Version: 01                                                       */
/* Description: Configuration file for I2C Driver for AVR ATmega32   */
/*********************************************************************/

/*Preprocessor Guard*/
#ifndef I2C_CONFIG_H 
#define I2C_CONFIG_H 

/*
  Macro to select the division factor for the bit rate generator
*/
#define I2C_BIT_RATE     (u8)0x07

/*
  Macro to specify state of the I2C Prescaler Bits
  Ranges: I2C_u8_PRESCALER_1
		  I2C_u8_PRESCALER_4
		  I2C_u8_PRESCALER_16
		  I2C_u8_PRESCALER_64
*/

#define I2C_PRESCALER_BITS   I2C_u8_PRESCALER_1

/*
  Macro to specify state of the Acknowledge Bit
  Ranges: I2C_u8_ENABLE
		  I2C_u8_DISABLE
*/
#define I2C_ACK_BIT_STATE    I2C_u8_ENABLE

/*
  Macro to specify state of the Two-wire Serial Interface Interrupt
  Ranges: I2C_u8_ENABLE
		  I2C_u8_DISABLE
*/
#define I2C_u8_INT_STATE		I2C_u8_DISABLE

/*Macro to determine the slave address of the controller in case if it becomes slave in the network*/
#define I2C_SLAVE_u8_ADDRESS        0x44

/*
  Macro to enable/disable the recognition of a General Call given over the Two-wire Serial Bus
  Ranges: I2C_u8_ENABLE
		  I2C_u8_DISABLE
*/
#define I2C_GEN_CALL_RECOG_STATE   I2C_u8_DISABLE

/* Macro to define the timeout which determines the fault time in the SPI upon transmitting or receiving */
#define I2C_u16_FAULT_TIMEOUT      (u16)50000

#endif /* I2C_CONFIG_H */ 

