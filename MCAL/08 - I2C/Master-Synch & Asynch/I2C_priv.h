/******************************************************************/
/* Author: MAhmoud Alaa Elwelily                                  */
/* Date: 18-May-19                                                */
/* Version: 01                                                    */
/* Description: Private file for I2C Driver for AVR ATmega32      */
/******************************************************************/

/*Preprocessor Guard*/
#ifndef I2C_PRIV_H 
#define I2C_PRIV_H 

/* Main functions used in the transmission operation */
#define GEN_START_COND()      SET_BIT(I2C_TWCR,TWSTA)
#define GEN_STOP_COND()       SET_BIT(I2C_TWCR,TWSTO)
#define CLEAR_I2C_INT_FLAG()  SET_BIT(I2C_TWCR,TWINT)

/* defining of Status Codes for Master Transmitter Mode */
#define START_SENT                0x08
#define REP_START_SENT            0x10
#define MT_SLA_ACK                0x18
#define MT_SLA_NACK               0x20
#define MT_DATA_ACK      	      0x28
#define MT_DATA_NACK      	      0x30

/* Defining I2C transmission states */
#define I2C_START                 0
#define I2C_SEND_ADDRESS          1
#define I2C_SEND_DATA             2
#define I2C_STOP                  3

/* Private macros to define the possible state of Two-wire Serial Interface Interrupta and the Ack Bit */
#define I2C_u8_ENABLE             (u8)0
#define I2C_u8_DISABLE            (u8)1

/* Private macros to define the available I2C Prescaler Bits */
#define I2C_u8_PRESCALER_1        (u8)0
#define I2C_u8_PRESCALER_4        (u8)1
#define I2C_u8_PRESCALER_16       (u8)2
#define I2C_u8_PRESCALER_64       (u8)3

/****************************************************************/
/* Defining I2C regisiters                                      */
/****************************************************************/
#define I2C_TWBR			((Register*) 0x20) -> ByteAccess
/*Defining the bits of I2C_TWBR register*/
#define TWBR0                 (u8)0
#define TWBR1                 (u8)1
#define TWBR2                 (u8)2
#define TWBR3	              (u8)3
#define TWBR4	              (u8)4
#define TWBR5                 (u8)5
#define TWBR6                 (u8)6
#define TWBR7                 (u8)7

#define I2C_TWCR			((Register*) 0x56) -> ByteAccess
/*Defining the bits of I2C_TWCR register*/
#define TWIE                  (u8)0
#define TWEN                  (u8)2
#define TWWC                  (u8)3
#define TWSTO                 (u8)4
#define TWSTA                 (u8)5
#define TWEA                  (u8)6
#define TWINT                 (u8)7

#define I2C_TWSR			((Register*) 0x21) -> ByteAccess
/*Defining the bits of I2C_TWSR register*/
#define TWPS0                 (u8)0
#define TWPS1                 (u8)1
#define TWS3                  (u8)3
#define TWS4                  (u8)4
#define TWS5                  (u8)5
#define TWS6                  (u8)6
#define TWS7                  (u8)7

#define I2C_TWDR			((Register*) 0x23) -> ByteAccess
/*Defining the bits of I2C_TWDR register*/
#define TWD0                  (u8)0
#define TWD1                  (u8)1
#define TWD2                  (u8)2
#define TWD3	              (u8)3
#define TWD4	              (u8)4
#define TWD5                  (u8)5
#define TWD6                  (u8)6
#define TWD7                  (u8)7

#define I2C_TWAR			((Register*) 0x22) -> ByteAccess
/*Defining the bits of I2C_TWAR register*/
#define TWGCE                  (u8)0

/************************************************************/
/*      ISRs of I2C interrupts                             */
/************************************************************/
/* The ISR of Two-wire Serial Interface Interrupt */
void __vector_19 (void) __attribute__((signal));

#endif /* I2C_PRIV_H */ 
