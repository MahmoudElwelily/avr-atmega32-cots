/*****************************************************************/
/* Author: MAhmoud Alaa Elwelily                                 */
/* Date: 18-May-19                                               */
/* Version: 01                                                   */
/* Description: Interface file for I2C Driver for AVR ATmega32   */
/*****************************************************************/

/*Preprocessor Guard*/
#ifndef I2C_INTERFACE_H 
#define I2C_INTERFACE_H 


/*
 * Description: Function to initialize the I2C peripheral
 * Inputs: none
 * Output: void
 */
void I2C_vInit (void);

/*
 * Description: SynchronousFunction to send a buffer using polling methodology on Two-wire Serial INT flag
 * Inputs: (1) pointer to array of characters needed to be sent
 * 		   (2) the size of the transmitted buffer
 * 		   (3) the address of the slave on which the master wants to write
 * Output: the error state of the function
 */
u8 I2C_u8SendSynch (u8 *Copy_pu8data,u8 Copy_u8BufferLength,u8 Copy_u8SlaveAddress);

/*
 * Description: Asynchronous Function to start sending a string using Two-wire Serial Interface Interrupt
 * Inputs: (1) pointer to array of characters needed to be sent
 * 		   (2) the size of the transmitted buffer
 * 		   (3) the address of the slave on which the master wants to write
 * 		   (4) the callback function to notify the upper layer that the complete message is successfully sent
 * Output: the Error state of the function
 */
u8 I2C_u8SendAsynch (u8 *Copy_pu8data,u8 Copy_u8BufferLength,u8 Copy_u8SlaveAddress, void (*Callback)(void));
#endif /* I2C_INTERFACE_H */ 
