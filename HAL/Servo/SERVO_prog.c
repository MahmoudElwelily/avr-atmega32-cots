/**************************************************************/
/* Author: Mahmoud Alaa Elwelily                              */
/* Date: 18-Apr-19                                            */
/* Version: 01                                                */
/* Description: Program file for SERVO Driver                 */
/**************************************************************/
#include "STD_TYPES.h" 
#include "BIT_CALC.h" 
#include "SERVO_interface.h" 
#include "TMR_interface.h"

u8 SERVO_u8SetServoAngle (u8 Copy_u8Angle)
{
	/*Local Variable holding the error state*/
	u8 Local_u8Error = ERROR_OK;
	u16 Local_u16RequiredCompareValue;

	if (Copy_u8Angle <= 180)
	{
		Local_u16RequiredCompareValue = (490+(11*Copy_u8Angle));
		TMR_u8SetPWM (Local_u16RequiredCompareValue);
	}
	else
	{
		Local_u8Error = ERROR_NOK;
	}
	/*Function return*/
	return Local_u8Error;
}
